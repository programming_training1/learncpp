#include <iostream>
#include <string>
using namespace std;

/*
 * Наследование
 *
 * Родительский/Базовый класс - клас от которого произошло наследование
 * Производный/дочерний класс, потомок, наследник - клас который унаследовал
 *
 */

class Human {
private:
    string name = "boboa";
public:
    string GetName() {
        return name;
    }
};

class Student : public Human {
public:
    string group;
    void Learn() {
        cout << "I learning!\n";
    }
};

class ExtramuralStudent : public Student {
public:
    void Learn() {
        cout << "I study less often then an ordinary student)\n";
    }
};


class Professor : public Human {
public:
    string subject;
};


int main() {
    Student st;
    Professor pr;
    ExtramuralStudent extraSt;
    cout << st.GetName();
    return 0;
}