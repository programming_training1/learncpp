#include <iostream>
#include <string>
using namespace std;
/*
 * Multiple inheritance
 */

class Car {
public:
    void Use() {
        cout << "I drive!" << endl;
    }
};

class Airplane {
public:
    void Use() {
        cout << "I fly!" << endl;
    }
};

class FlyingCar : public Airplane, public Car {
public:

};

int main() {
    FlyingCar fc;
    ((Car)fc).Use();

    return 0;
}