#include <iostream>
#include <string>
using namespace std;

class Human {
public:
    string name;
    int weight;
    int age;

    void print() {
        cout << name << endl;
        cout << weight << endl;
        cout << age << endl;
    }
};

class point {
public:
    int x;
    void print() {
        cout << x << endl;
        cout << y << endl;
        cout << z << endl;
        print_y();
    }

private:
    int y; // Не доступно у наследованных классов. Но доступна для дружественных классов и функций.
    void print_y() {
        cout << y << endl;
    }

protected:
    int z;
};

int main() {
    point a;
    a.x = 11;
    a.print();


    return 0;
}
