#include <iostream>
#include <string>
using namespace std;

// order of calling constructors during inheritance
// order of calling destructors during inheritance
// calling the base class constructor from the derived class constructor

class A {
public:
    A() {
        cout << "Calling class A's constructor" << endl;
    }

    A(string msg) {
        this->msg = msg;
    }

    void PrintMsg() {
        cout << msg << endl;
    }

    ~A() {
        cout << "Calling class A's destructor" << endl;
    }

private:
    string msg;
};

class B : public A {
public:
    B(){
        cout << "Calling class B's constructor" << endl;
    }
    ~B() {
        cout << "Calling class B's destructor" << endl;
    }
};

class C : public B {
public :
    C() {
        cout << "Calling class C's constructor" << endl;
    }
    ~C() {
        cout << "Calling class C's destructor" << endl;
    }
};

int main() {
    B b;
    b.PrintMsg();
    return 0;
}
