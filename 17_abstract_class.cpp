#include <iostream>
using namespace std;

/*
 * Abstract class
 * Pure virtual function
 * Virtual
 * Override
 */

class Weapon
{
public:
    void hello() {
        cout << "hell\n";
    }
    virtual void Shoot() = 0; // Pure virtual function
};

class Gun : public Weapon
{
public :
    void Shoot() override
    {
        cout << "bang!" << endl;
    }
};

class SubmachineGum: public Weapon
{
public:
    void Shoot() override
    {
        cout << "bang bang bang!" << endl;
    }
};

class Bazooka : public Weapon
{
public:
    void Shoot() override
    {
        cout << "Booom!!!" << endl;
    }
};

class Player
{
public:
    void Shoot(Weapon *weapon)
    {
        weapon->Shoot();
    }
};


int main() {
    Gun gun;
    SubmachineGum subgun;
    Bazooka bazooka;

    Player p;
    p.Shoot(&bazooka);

    return 0;
}

/*
 * 0. Класс содержащий чисто виртуальную функцию является абстрактным
 * 1. Нельзя создавать объект абстрактного класса
 * 2. При создании потомка от абстрактного класса мы должны переопределить
 * чисто виртуальные функции
 *
 *
 */