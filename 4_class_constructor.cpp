#include <iostream>
using namespace std;

class Point {

private:
    int x;
    int y;

public:
    // Конструктор класса (вызывается сам)
    Point(int val_x, int val_y) {
        x = val_x;
        y = val_y;
    }

    int GetX() {
        return x;
    }
    int GetY() {
        return y;
    }
    void SetX(int val_x) {
        x = val_x;
    }
    void SetY(int val_y) {
        y = val_y;
    }

    void Print() {
        cout << "X = " << x << endl;
        cout << "Y = " << y << endl;
    }
};



int main() {
    /* Point a; <- Это ошибка. Если не указать конструктор класса, то он будет указан по умолчанию
     * таким образом self() {}. И Point a; не будет ошибкой
    */

    Point a(1, 3);
    a.Print();

    return 0;
}