#include <iostream>
using namespace std;
/*
 * Polymorphism
 * Virtual functions
 * virtual
 * override
 */


class Gun {
public:
    virtual void Shoot() {
        cout << "BANG!" << endl;
    }
};


class SubmachineGun : public Gun {
public:
    void Shoot() override {
        cout << "BANG! BANG! BANG!" << endl;
    }
};


class Player {
public:
    void Shoot(Gun *gun) {
        gun->Shoot();
    }
};


int main() {

    Gun gun;
    SubmachineGun machinegun;
    Gun *weopon1 = &gun;
    Gun *weopon2 = &machinegun; // correct
//    weopon2->Shoot();

    Player p;
    p.Shoot(weopon2);
    return 0;
}

/*
 * Virtual и override нужны, чтобы переопределить функцию.
 * Без них этого сделать не получится
 * (но может и получится, только компилятор не будет это отслеживать)
 *
 * Также при указании этих ключевых слов при использовании указателя
 * на родительский класс функция будет переопределена.
 * А если не указывать, то она не будет переопределена.
 *
 * То есть это очень удобно в случае передачи указателей на классы
 *
 * А указатели удабно передовать в ооп потому что классы становятся очень большими
 * и дешевле по памяти передать их указатель
 *
 */