#include <iostream>
#include <string>
using namespace std;

/*
 * Delegating constructor
 */

class Human {
public:
     Human(string Name) {
         this->Name = Name + "!";
         this->Age = 0;
         this->Weight = 0;
     }

    Human(string Name, int Age):Human(Name) {
         this->Age = Age;
     }

    Human(string Name, int Age, int Weight):Human(Name, Age)  {
        this->Weight = Weight;
    }

    void Print() {
         cout << Name << " " << Age << " " << Weight << endl;
     }

    string Name;
    int Age;
    int Weight;
};

int main() {
    Human a("Rail ", 2);
    a.Print();
    return 0;
}