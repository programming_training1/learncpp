#include <iostream>
#include <string>
using namespace std;
/*
 * Multiple inheritance
 */

class Car {
public:
    Car() {
        cout << "car constructor call" << endl;
    }
    ~Car() {
        cout << "car destructor call" << endl;
    }
    string str = "abobobo";
    void Drive() {
        cout << "i drive" << endl;
    }
};

class Airplane {
public:
    Airplane() {
        cout << "airplane constructor call" << endl;;
    }
    ~Airplane() {
        cout << "airplane destructor call" << endl;
    }
    string str = "a;skldfa";
    void Fly() {
        cout << "i fly" << endl;
    }
};

class FlyingCar : public Airplane, public Car {
public:
    FlyingCar() {
        cout << "flying car constructor call" << endl;
    }
    ~FlyingCar() {
        cout << "flying car destructor call" << endl;
    }

};

int main() {
    FlyingCar fc;

    return 0;
}