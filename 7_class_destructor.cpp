#include <iostream>
using namespace std;


/* Деструктор класса */

class Point {

private:
    int x;
    int y;

public:

    // Конструктор класса (вызывается сам)
    Point()
    {
        x = 0;
        y = 0;
    }

    Point(int val_x, int val_y) {
        x = val_x;
        y = val_y;
    }

    int GetX() {
        return x;
    }
    int GetY() {
        return y;
    }
    void SetX(int val_x) {
        x = val_x;
    }
    void SetY(int val_y) {
        y = val_y;
    }

    void Print() {
        cout << "X = " << x << " ";
        cout << "Y = " << y << endl;
    }
};

class MyClass
{
    int data;
public:
    MyClass(int val) {
        data = val;
        cout << "Вызвался конструктор " << data << endl;
    }

    ~MyClass() {
        cout << "Вызвался деструктор " << data << endl;
    }
};

void Foo() {
    cout << "Foo начало выполнения" << endl;
    MyClass a(1);
    MyClass b(2);
    cout << "Foo конец выполнения" << endl;
}

int main() {
    Foo();


    return 0;
}