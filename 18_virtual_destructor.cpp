#include <iostream>
using namespace std;

/*
 * Virtual destructor
 */

class A {
public:
    A() {
        cout << "Dynamic memory of class A object is allocated" << endl;
    }
    virtual ~A() {
        cout << "Dynamic memory of class A object is free" << endl;
    }
};

class B: public A {
public:
    B() {
        cout << "Dynamic memory of class B object is allocated" << endl;
    }
    ~B() override {
        cout << "Dynamic memory of class B object is free" << endl;
    }
};

int main() {
    A *bptr = new B;
    delete bptr;


    return 0;
}