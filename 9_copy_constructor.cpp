#include <iostream>
using namespace std;


/* при копировании объектов такого класса по умолчанию происходит ошибка памяти
class MyClass {
    int *data;
public:
    MyClass(int size)
    {
        this->data = new int[size];

        for (int i = 0; i < size; i++) {
            data[i] = i;
        }

        cout << "Вызвался конструктор " << this << endl;
    }

    ~MyClass() {
        cout << "Вызвался диструктор " << this << endl;
        delete[] data;
    }
};

void Foo1(MyClass value)
{
    cout << "Foo1" << endl;
}
*/

class MyClass {
    int size;
public:
    int *data;
    MyClass(int size)
    {
        this->size = size;
        this->data = new int[size];

        for (int i = 0; i < size; i++) {
            data[i] = i;
        }

        cout << "Вызвался конструктор " << this << endl;
    }

    MyClass(const MyClass &other)
    {
        // this->data = other.data Это конструктор копирования по умолчанию
        this->size = other.size;
        this->data = new int[other.size];
        cout << "Вызвался коструктор копированя " << this << endl;

    }

    ~MyClass() {
        cout << "Вызвался диструктор " << this << endl;
        delete[] data;
    }
};

void Foo1(MyClass value)
{
    cout << "Foo1" << endl;
}

MyClass Foo2()
{
    cout << "Foo2" << endl;
    MyClass temp(2);
    return temp;
}

int main()
{
    MyClass a(1);
    MyClass b(a);
}
