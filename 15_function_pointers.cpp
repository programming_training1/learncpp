#include <iostream>
#include <string>
using namespace std;

/*
 * function pointers
 *
 * type funcs(*name pointer)(list of parameters);
 *
 *
 */

//int Foo1(int a) {
//    cout << "void Foo 1 ()" << endl;
//    return a - 1;
//}
//
//int Foo2(int a)  {
//    cout << "void Foo 2 ()" << endl;
//    return a*2;
//}

string DataFromBD() {
    return "Data From BD";
}

string DataFromWebServer() {
    return "Data From WebServer";
}

void ShowInfo(string (*funcPtr)()) {
    cout << funcPtr();
}

int main() {
//    int (*fooPointer)(int a) = Foo1;
//    int (*fooPointer2)(int a) = Foo2;
//    cout << fooPointer(3) << endl;
//    cout << fooPointer2(3) << endl;
    ShowInfo(DataFromWebServer);


    return 0;
}