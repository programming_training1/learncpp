#include <iostream>
using namespace std;

/* Перегрузка функций */

int Sum1(int a, int b) {
    cout << "int" << endl;
    return a + b;
}

double Sum1(double a, double b) {
    cout << "double" << endl;
    return a + b;
}

int Sum1(int a, int b, int c) {
    cout << "int3" << endl;
    return a + b + c;
}


/* Шаблоны функций */

template <class T>
void Sum2 (T a) {
    cout << a << endl;
}

template <typename T1, typename T2>
void Sub2 (T1 a, T2 b) {
    cout << a << endl;
    cout << b << endl;
}

int main() {

    double t = Sum1(3, 134, 3);
    cout << t << endl;


    return 0;
}