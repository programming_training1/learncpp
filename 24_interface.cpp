#include <iostream>
#include <string>
using namespace std;

/*
 * Interface is abstract class in which all methods are purely virtual
 */

class IBicycle {
public:
    void virtual TwistTheWheel() = 0;
    void virtual Ride() = 0;
};

class SimpleBicycle : public IBicycle {
public :
    void TwistTheWheel() override {
        cout << "TWIST" << endl;
    }
    void Ride() override {
        cout << "RIDE" << endl;
    }
};



class Human {
public:
    void RideOn(IBicycle &bicycle) {
        cout << "twist the wheel" << endl;
        bicycle.TwistTheWheel();
        cout << endl << "go" << endl;
        bicycle.Ride();
    }
};

int main() {
    Human h;
    SimpleBicycle b;

    h.RideOn(b); //?
}