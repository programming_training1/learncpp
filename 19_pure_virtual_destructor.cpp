#include <iostream>
using namespace std;
/*
 * Pure virtual destructor -> abstract class
 */

class A {
public:
    A() {
        cout << "Dynamic memory of class A object is allocated" << endl;
    }
    virtual ~A() = 0;
};

A::~A() {}; // <- Нужно добавить так как при разрушении объекта класса B
// также вызовется деструктор класса А

class B: public A {
public:
    B() {
        cout << "Dynamic memory of class B object is allocated" << endl;
    }
    ~B() override {
        cout << "Dynamic memory of class B object is free" << endl;
    }
};

int main() {
    A *bptr = new B;
    delete bptr;


    return 0;
}