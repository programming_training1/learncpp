#include <iostream>
using namespace std;

/* Ключевое слово this */

/* Ключевое слово this - это указатель объекта на самого себя.
 * (Это знание объекта о том где он находится в памяти)
 *
*/
class Point {
private:
    int x;
    int y;

public:
    Point()
    {
        x = 0;
        y = 0;
        cout << "this = " << this << endl;
    }

    Point(int val_x, int val_y)
    {
        x = val_x;
        y = val_y;
        cout << "this = " << this << endl;
    }
    int GetX()
    {
        return x;
    }
    int GetY() {
        return y;
    }
    void SetX(int val_x) {
        x = val_x;
    }
    void SetY(int y) {
        this->y = y;
    }

    void Print() {
        cout << "X = " << x << " ";
        cout << "Y = " << y << endl;

    }


};

int main()
{
    Point a;
    a.SetY(5);
    a.Print();

    return 0;
}



