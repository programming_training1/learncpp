#include <iostream>
using namespace std;

/* Инкапсуляция - принцип ооп, который представляет собой упаковку данных и методов,
 * внутри одной сущности, называемой классом. Она обеспечивает скрытие деталей реализации и
 * предоставляет интерфейс для взаимодействия с этой сущностью.
 *
 * Основной идеей инкапсуляции является ограничение доступа к членам класса.
 *
*/

// Пример инкапсуляции (интерфейс взаимодействия)
class Point {

private:
    int x;
    int y;

public:
    int GetX() {
        return x;
    }

    void SetX(int val_x) {
        x = val_x;
    }

    void Print() {
        cout << "X = " << x << endl;
        cout << "Y = " << y << endl;
    }
};

// Пример инкапсуляции (скрытие некоторого функционала)
class CoffeeGrinder {
private:
    bool CheckVoltage() {
        return false;
    }

public:
    void Start() {
        if (CheckVoltage()) {
            cout << "vjuHH" << endl;
        } else {
            cout << "beep beep" << endl;
        }
    }
};


int main() {
   CoffeeGrinder a;
   a.Start();


    return 0;
}
