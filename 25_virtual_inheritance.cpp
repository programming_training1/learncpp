#include <iostream>
#include <string>
using namespace std;

class Component {
public:
    Component(string companyName) {
        cout << "constructor Component " << endl;
        this->companyName = companyName;
    }
    string companyName;
};

class GPU : public Component {
public:
    GPU(string companyName) : Component(companyName) {
        cout << "GPU " << endl;
    }
};

class Memory : public Component {
public:
    Memory(string companyName) : Component(companyName) {
        cout << "Memory " << endl;
    }
};

class GraphicCard : public GPU, public Memory {
public:
    GraphicCard(string GPUCompanyName, string MemoryCompanyName):GPU(GPUCompanyName), Memory(MemoryCompanyName) {
        cout << "GraphicCard ";
    }
};

class Character {
public:
    int HP;
    Character() {
        cout << "char" << endl;
    };
};

class Orc : public virtual Character { // <-
public:
    Orc() {
        cout << "orc" << endl;
    }
};

class Warrior : public virtual Character { // <-
public:
    Warrior() {
        cout << "war" << endl;
    }
};

class OrcWarrior : public Orc, public Warrior {
public:
    OrcWarrior() {
        cout << "orc war" << endl;
    }
};


int main() {
//    GraphicCard a("intel", "kingston  "); <- это будет работать хорошо
    OrcWarrior b; // <- без виртуального наследования переменная hp
    // не будет корректно работать в этом прмере
    return 0;
}

