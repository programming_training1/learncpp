#include <iostream>
#include <string>
using namespace std;

class Msg {
public:
    Msg(string msg) {
        this->msg = msg;
    }
    virtual string GetMsg() {
        return msg;
    }

private:
    string msg;
};

class BreaketsMsg : public Msg {
public:
    BreaketsMsg(string msg) : Msg(msg) {

    }
    string GetMsg() override {
        return  "[" + Msg::GetMsg() + "]";
    }
};

class Printer {
public:
    void Print(Msg *msg) {
        cout << msg->GetMsg() << endl;
    }
};


int main() {
    Msg a("hello");
    BreaketsMsg b("ab");
    Printer p;
    p.Print(&b);
}
