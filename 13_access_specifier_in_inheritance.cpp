#include <iostream>
#include <string>
using namespace std;

class A {
public:
    string msgOne = "msg one";


private:
    string msgTwo = "msg two";



protected:
    string msgTree = "msg three";
};

class B : private A {
public:
    void PrintMsg() {
        cout << msgOne << endl;
    }
};

class C : public B {
public:
    void PrintMsg() {
        cout << msgOne << endl;
    }
};

int main() {
    B b;
    b.PrintMsg();

    C c;
    return 0;
}

